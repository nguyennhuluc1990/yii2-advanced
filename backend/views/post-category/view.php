<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use common\models\PostCategory;

/* @var $this yii\web\View */
/* @var $model common\models\PostCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Post Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="post-category-view">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'bootstrap' => true,
                        'hover' => true,
                        'panel' => [
                            'heading' => $this->title,
                            'type' => DetailView::TYPE_INFO
                        ],
                        'attributes' => [
                            [
                                'attribute' => 'id',
                                'value' => $model->id,
                                'displayOnly' => true
                            ],
                            'name',
                            'description',
                            'keywords',
                            [
                                'attribute' => 'banner',
                                'format'    => 'html',
                                'value' => '<div class="file-default-preview"><img src="' . $model->banner . '" alt=""></div>'
                            ],
                            [
                                'attribute' => 'poster',
                                'format'    => 'html',
                                'value' => '<div class="file-default-preview"><img src="' . $model->poster . '" alt=""></div>'
                            ],
                            [
                                'attribute' => 'status',
                                'value' => PostCategory::$status_list[$model->status],
                                'type' => DetailView::INPUT_SELECT2,
                                'widgetOptions' => [
                                    'data' => PostCategory::$status_list,
                                    'options' => ['placeholder' => 'Select ...'],
                                    'pluginOptions' => ['allowClear' => true, 'width' => '100%'],
                                ],
                            ],
                            [
                                'attribute' => 'created_at',
                                'format' => ['datetime', 'format' => 'php:d-m-Y g:i A'],
                                'displayOnly' => true
                            ],
                            [
                                'attribute' => 'updated_at',
                                'format' => ['datetime', 'format' => 'php:d-m-Y g:i A'],
                                'displayOnly' => true
                            ],
                        ],
                        'deleteOptions' => [
                            'url' => ['delete', 'id' => $model->id],
                        ],
                        'enableEditMode' => false,
                        'alertWidgetOptions' => false
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>