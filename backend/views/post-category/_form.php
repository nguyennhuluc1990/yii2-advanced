<?php

use common\helpers\CUtils;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use insolita\wgadminlte\LteBox;
use insolita\wgadminlte\LteConst;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PostCategory */
/* @var $form yii\widgets\ActiveForm */

LteBox::begin([
    'type' => LteConst::TYPE_DEFAULT,
    'isSolid' => true,
    'tooltip' => 'this tooltip description',
    'title' => 'Manage Post Category',
]);

?>

<div class="post-category-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'keywords')->widget(Select2::classname(), [
        'data' => CUtils::dataTags($model->keywords),
        'options' => ['placeholder' => 'Add tag new ...', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
            'maximumInputLength' => 50
        ],
    ]) ?>

    <div class="form-group highlight-addon">
        <label class="control-label has-star col-md-2"><?php echo $model->getAttributeLabel('banner'); ?></label>
        <div class="col-md-10" id="uploadArticle">
            <?= Html::activeHiddenInput($model, 'banner', ['id' => 'ArticleImage']); ?>
            <div class="well-small">
                <?php
                $image = ($model->banner) ? $model->banner : '/img/noimage.gif';
                ?>
                <?= FileInput::widget([
                    'name' => 'file',
                    'pluginOptions' => [
                        'showCaption' => false,
                        'showRemove' => true,
                        'showClose' => false,
                        'showUpload' => true,
                        'browseClass' => 'btn btn-primary',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' => 'Select Photo',
                        'uploadUrl' => Url::toRoute(['upload/tmp']),
                        'defaultPreviewContent' => Html::img($image),
                        'maxFileSize' => 1500,
                        'minImageWidth' => 200,
                        'minImageHeight' => 200,
                    ],
                    'options' => [
                        'accept' => 'image/*',
                        'allowedFileExtensions' => ['jpg', 'gif', 'png']
                    ],
                    'pluginEvents' => [
                        'fileuploaded' => 'function(event, data, previewId, index){
                             var response = data.response;
                             if(response.file_name!=""){
                                $("#ArticleImage").val(response.file_name);
                                $(".kv-upload-progress").remove();
                                $(".file-thumb-progress").remove();
                             }else{
                                alert(response.message);
                             }
                             return false;
                        }',
                        'filesuccessremove' => 'function(event, id){}'
                    ]
                ]); ?>
            </div>
        </div>
    </div>

    <div class="form-group highlight-addon">
        <label class="control-label has-star col-md-2"><?php echo $model->getAttributeLabel('poster'); ?></label>
        <div class="col-md-10" id="uploadArticle1">
            <?= Html::activeHiddenInput($model, 'poster', ['id' => 'ArticleImage1']); ?>
            <div class="well-small">
                <?php
                $image = ($model->poster) ? $model->poster : '/img/noimage.gif';
                ?>
                <?= FileInput::widget([
                    'name' => 'file',
                    'pluginOptions' => [
                        'showCaption' => false,
                        'showRemove' => true,
                        'showClose' => false,
                        'showUpload' => true,
                        'browseClass' => 'btn btn-primary',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' => 'Select Photo',
                        'uploadUrl' => Url::toRoute(['upload/tmp']),
                        'defaultPreviewContent' => Html::img($image),
                        'maxFileSize' => 1500,
                        'minImageWidth' => 200,
                        'minImageHeight' => 200,
                    ],
                    'options' => [
                        'accept' => 'image/*',
                        'allowedFileExtensions' => ['jpg', 'gif', 'png']
                    ],
                    'pluginEvents' => [
                        'fileuploaded' => 'function(event, data, previewId, index){
                             var response = data.response;
                             if(response.file_name!=""){
                                $("#ArticleImage").val(response.file_name);
                                $(".kv-upload-progress").remove();
                                $(".file-thumb-progress").remove();
                             }else{
                                alert(response.message);
                             }
                             return false;
                        }',
                        'filesuccessremove' => 'function(event, id){}'
                    ]
                ]); ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'slug')->textInput() ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

    <?php ActiveForm::end();
    LteBox::end(); ?>

</div>
