<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .grid-view td {
        white-space: initial;
        max-width: 500px;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
                <p>
                    <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Tạo mới'), ['create'], ['id' => 'add_more', 'class' => 'btn btn-primary']) ?>
                </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            'email:email',
            //'phone',
            //'role_id',
            //'status',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => \common\models\User::$arrStatus,
                'value' => function ($data) {
                    return \common\models\User::$arrStatus[$data->status];
                }
            ],
            'created_at:datetime',
            //'updated_at',
            //'avatar',
            //'birthday',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
            </div>
        </div>
    </div>
</div>
