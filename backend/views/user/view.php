<?php

use common\models\CustomerUser;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <p>
                    <?= Html::a(Yii::t('app', 'Đặt lại mật khẩu'), ['reset-password', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Cập nhật'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Xóa'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Bạn có chắc bạn muốn xóa mục này?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            [
                'attribute' => 'avatar',
                'format'    => 'html',
                'value' => '<div class="file-default-preview"><img src="' . $model->avatar . '" alt=""></div>'
            ],
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            'email:email',
            'phone',
//            'role_id',
//            'status',
            'birthday:datetime',
            [
                'attribute' => 'status',
                'filter' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return \common\models\User::$arrStatus[$data->status];
                },
            ],
            'created_at:datetime',
//            'updated_at',
//            'avatar',
//            'birthday',
        ],
    ]) ?>
            </div>
        </div>
    </div>
</div>
