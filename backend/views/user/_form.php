<?php

use common\models\UserRole;
use insolita\wgadminlte\LteBox;
use insolita\wgadminlte\LteConst;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$new_list_role = UserRole::find()->all();
$list_role = ArrayHelper::map($new_list_role, 'id', 'name');

LteBox::begin([
    'type' => LteConst::TYPE_DEFAULT,
    'isSolid' => true,
    'tooltip' => 'this tooltip description',
    'title' => 'Quản lý tài khoản',
]);
?>


<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <!-- /.box-header -->
            <div class="box-body">

                <?php $form = \kartik\widgets\ActiveForm::begin([
                    'type' => ActiveForm::TYPE_HORIZONTAL,
                    'options' => [
                        'enctype' => 'multipart/form-data'
                    ]
                ]); ?>

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?php
                if ($model->isNewRecord) {
                    echo $form->field($model, 'password_hash')->passwordInput(['placeholder' => 'Enter password...', 'maxlength' => 100])->label(Yii::t('app', 'Mật khẩu'));
                    echo $form->field($model, 'confirm_password')->passwordInput(['placeholder' => 'Enter confirm password...', 'maxlength' => 100]);
                }
                ?>

                <?= $form->field($model, 'role_id')->dropDownList($list_role, ['prompt' => 'Select....']); ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                <div class="form-group highlight-addon">
                    <label class="control-label has-star col-md-2"><?php echo $model->getAttributeLabel('avatar'); ?></label>
                    <div class="col-md-10" id="uploadArticle">
                        <?= Html::activeHiddenInput($model, 'avatar', ['id' => 'ArticleImage']); ?>
                        <div class="well-small">
                            <?php
                            $image = ($model->avatar) ? $model->avatar : '/img/noimage.gif';
                            ?>
                            <?= FileInput::widget([
                                'name' => 'file',
                                'pluginOptions' => [
                                    'showCaption' => false,
                                    'showRemove' => true,
                                    'showClose' => false,
                                    'showUpload' => true,
                                    'browseClass' => 'btn btn-primary',
                                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                                    'browseLabel' => 'Select Photo',
                                    'uploadUrl' => Url::toRoute(['upload/tmp']),
                                    'defaultPreviewContent' => Html::img($image),
                                    'maxFileSize' => 1500,
                                    'minImageWidth' => 200,
                                    'minImageHeight' => 200,
                                ],
                                'options' => [
                                    'accept' => 'image/*',
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png']
                                ],
                                'pluginEvents' => [
                                    'fileuploaded' => 'function(event, data, previewId, index){
                             var response = data.response;
                             if(response.file_name!=""){
                                $("#ArticleImage").val(response.file_name);
                                $(".kv-upload-progress").remove();
                                $(".file-thumb-progress").remove();
                             }else{
                                alert(response.message);
                             }
                             return false;
                        }',
                                    'filesuccessremove' => 'function(event, id){}'
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>

                <?= $form->field($model, 'birthday')->widget(DatePicker::className(), [
                    'options' => [
                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                        'removeButton' => false,
                        'placeholder' => 'Ngày sinh',
                        'value' => (is_integer($model->birthday)) ? date('d-m-Y', $model->birthday) : ''
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy'
                    ],
                ]) ?>

                <?= $form->field($model, 'status')->dropDownList(\common\models\User::$arrStatus, ['prompt' => 'Chọn trạng thái']) ?>

                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Tạo mới') : Yii::t('app', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

                <?php \kartik\widgets\ActiveForm::end();
                LteBox::end(); ?>
            </div>
        </div>
    </div>
</div>
