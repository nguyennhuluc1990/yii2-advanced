<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\detail\DetailView;
use common\models\Menu;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="menu-view">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'bootstrap' => true,
                        'hover' => true,
                        'panel' => [
                            'heading' => $this->title,
                            'type' => DetailView::TYPE_INFO,
                        ],
                        'attributes' => [
                            'id',
                            'name',
                            'link',
                            [
                                'attribute' => 'target',
                                'value' => Menu::$target_list[$model->target],
                                'type' => DetailView::INPUT_SELECT2,
                                'widgetOptions' => [
                                    'data' => Menu::$target_list,
                                    'options' => ['placeholder' => 'Select ...'],
                                    'pluginOptions' => ['allowClear' => true, 'width' => '100%'],
                                ],
                            ],
                            'type',
                            'parent_id',
                            [
                                'attribute' => 'status',
                                'value' => Menu::$status_list[$model->status],
                                'type' => DetailView::INPUT_SELECT2,
                                'widgetOptions' => [
                                    'data' => Menu::$status_list,
                                    'options' => ['placeholder' => 'Select ...'],
                                    'pluginOptions' => ['allowClear' => true, 'width' => '100%'],
                                ],
                            ],
                            [
                                'attribute' => 'created_at',
                                'format' => ['datetime', 'format' => 'php:d-m-Y g:i A'],
                                'displayOnly' => true
                            ],
                            [
                                'attribute' => 'updated_at',
                                'format' => ['datetime', 'format' => 'php:d-m-Y g:i A'],
                                'displayOnly' => true
                            ],
                        ],
                        'deleteOptions' => [
                            'url' => ['delete', 'id' => $model->id],
                        ],
                        'enableEditMode' => false,
                        'alertWidgetOptions' => false
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
