<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use common\models\Menu;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="menu-index">
                    <?php
                    // echo $this->render('_search', ['model' => $searchModel]);
                    Pjax::begin();
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'name',
                            'link',
                            'target',
//                            'type',
                            //'parent_id',
                            [
                                'attribute' => 'status',
                                'value' => function ($model, $key, $index, $widget) {
                                    return Menu::$status_list[$model->status];
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => Menu::$status_list,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => ['placeholder' => 'Any status'],
                                'format' => 'raw'
                            ],
                            //'created_at',
                            //'updated_at',
                            //'created_by',
                            //'updated_by',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'responsive' => true,
                        'hover' => true,
                        'condensed' => true,
                        'floatHeader' => true,
                        'panel' => [
                            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
                            'type' => 'info',
                            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                            'showFooter' => false
                        ],
                    ]); ?>


                </div>
            </div>
        </div>
    </div>
</div>