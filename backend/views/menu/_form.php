<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use insolita\wgadminlte\LteBox;
use insolita\wgadminlte\LteConst;
use common\models\Menu;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */

$new_list_menu = Menu::find()->all();
$list_menu = ArrayHelper::map($new_list_menu, 'id', 'name');

LteBox::begin([
    'type' => LteConst::TYPE_DEFAULT,
    'isSolid' => true,
    'tooltip' => 'this tooltip description',
    'title' => 'Manage Menu',
]);
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'target')->dropDownList(Menu::$target_list, ['prompt' => 'Select....']) ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'parent_id')->dropDownList($list_menu, ['prompt' => 'Select....']) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

    <?php ActiveForm::end(); LteBox::end();?>

</div>
