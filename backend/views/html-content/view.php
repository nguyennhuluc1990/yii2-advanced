<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use common\models\HtmlContent;

/* @var $this yii\web\View */
/* @var $model common\models\HtmlContent */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Html Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="html-content-view">
                    <?= DetailView::widget([
                        'model' => $model,
                        'condensed' => false,
                        'bootstrap' => true,
                        'hover' => true,
                        'panel' => [
                            'heading' => $this->title,
                            'type' => DetailView::TYPE_INFO,
                        ],
                        'attributes' => [
                            'id',
                            'code',
                            'content:html',
                            [
                                'attribute' => 'status',
                                'value' => HtmlContent::$status_list[$model->status],
                            ],
                            'created_at:datetime',
                            'updated_at:datetime',
                        ],
                        'deleteOptions' => [
                            'url' => ['delete', 'id' => $model->id],
                        ],
                        'enableEditMode' => false,
                        'alertWidgetOptions' => false
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>
