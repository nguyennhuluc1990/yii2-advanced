<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HtmlContent */

$this->title = Yii::t('app', 'Create Html Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Html Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="html-content-create">
    <div class="panel panel-info">
        <div class="box ">
            <div class="box-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
