<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use common\models\HtmlContent;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HtmlContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Html Contents');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .grid-view td {
        white-space: initial;
        max-width: 500px;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="html-content-index">

                    <?php
                    // echo $this->render('_search', ['model' => $searchModel]);
                    Pjax::begin();
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'code',
                            [
                                'attribute' => 'status',
                                'value' => function ($model, $key, $index, $widget) {
                                    return HtmlContent::$status_list[$model->status];
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => HtmlContent::$status_list,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => ['placeholder' => 'Any status'],
                                'format' => 'raw'
                            ],
                            'created_at:datetime',
                            //'updated_at',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                        'responsive' => true,
                        'hover' => true,
                        'condensed' => true,
                        'floatHeader' => true,
                        'panel' => [
                            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
                            'type' => 'info',
                            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                            'showFooter' => false
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
