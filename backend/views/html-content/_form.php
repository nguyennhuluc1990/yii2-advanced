<?php

use insolita\wgadminlte\LteBox;
use insolita\wgadminlte\LteConst;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use xvs32x\tinymce\Tinymce;

/* @var $this yii\web\View */
/* @var $model common\models\HtmlContent */
/* @var $form yii\widgets\ActiveForm */

LteBox::begin([
    'type' => LteConst::TYPE_DEFAULT,
    'isSolid' => true,
    'tooltip' => 'this tooltip description',
    'title' => 'Manage Post',
]);
?>

<div class="html-content-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL
    ]); ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(Tinymce::className(), [
        //TinyMCE options
        'pluginOptions' => [
            'plugins' => [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            'toolbar1' => "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            'toolbar2' => "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor ",
            'image_advtab' => true,
            'filemanager_title' => "Filemanager",
        ],
        //Widget Options
        'fileManagerOptions' => Yii::$app->params['fileManagerOptions']
    ])
    ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

    <?php ActiveForm::end(); LteBox::end();?>

</div>
