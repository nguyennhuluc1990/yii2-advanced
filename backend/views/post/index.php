<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use common\models\Post;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .grid-view td {
        white-space: initial;
        max-width: 500px;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="post-index">
                    <?php
                    // echo $this->render('_search', ['model' => $searchModel]);
                    Pjax::begin();
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

//                            'id',
                            'title',
                            'description:ntext',
//                            'content:ntext',
//                            'banner',
                            //'poster',
                            //'keywords',
                            //'post_category_id',
                            [
                                'attribute' => 'status',
                                'value' => function ($model, $key, $index, $widget) {
                                    return Post::$status_list[$model->status];
                                },
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => Post::$status_list,
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => ['placeholder' => 'Any status'],
                                'format' => 'raw'
                            ],
                            //'is_deleted',
                            //'created_at',
                            //'updated_at',
                            //'created_by',
                            //'updated_by',

                            'created_at:datetime',
                            [
                                'class' => 'yii\grid\ActionColumn',
                            ],
                        ],
                        'responsive' => true,
                        'hover' => true,
                        'condensed' => true,
                        'floatHeader' => true,
                        'panel' => [
                            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
                            'type' => 'info',
                            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                            'showFooter' => false
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>