<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\select2\Select2;
use insolita\wgadminlte\LteBox;
use insolita\wgadminlte\LteConst;
use common\models\PostCategory;
use yii\helpers\Url;
use common\helpers\CUtils;
use xvs32x\tinymce\Tinymce;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */

$new_list_category = PostCategory::findAll(['is_deleted' => PostCategory::NOT_DELETED]);
$list_category = ArrayHelper::map($new_list_category, 'id', 'name');

LteBox::begin([
    'type' => LteConst::TYPE_DEFAULT,
    'isSolid' => true,
    'tooltip' => 'this tooltip description',
    'title' => 'Manage Post',
]);
?>

<div class="post-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->widget(Tinymce::className(), [
        //TinyMCE options
        'pluginOptions' => [
            'plugins' => [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            ],
            'toolbar1' => "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            'toolbar2' => "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor ",
            'image_advtab' => true,
            'filemanager_title' => "Filemanager",
        ],
        //Widget Options
        'fileManagerOptions' => Yii::$app->params['fileManagerOptions']
    ])
    ?>

    <div class="form-group highlight-addon">
        <label class="control-label has-star col-md-2"><?php echo $model->getAttributeLabel('banner'); ?></label>
        <div class="col-md-10" id="uploadArticle">
            <?= Html::activeHiddenInput($model, 'banner', ['id' => 'ArticleImage']); ?>
            <div class="well-small">
                <?php
                $image = ($model->banner) ? $model->banner : '/img/noimage.gif';
                ?>
                <?= FileInput::widget([
                    'name' => 'file',
                    'pluginOptions' => [
                        'showCaption' => false,
                        'showRemove' => true,
                        'showClose' => false,
                        'showUpload' => true,
                        'browseClass' => 'btn btn-primary',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' => 'Select Photo',
                        'uploadUrl' => Url::toRoute(['upload/tmp']),
                        'defaultPreviewContent' => Html::img($image),
                        'maxFileSize' => 1500,
                        'minImageWidth' => 200,
                        'minImageHeight' => 200,
                    ],
                    'options' => [
                        'accept' => 'image/*',
                        'allowedFileExtensions' => ['jpg', 'gif', 'png']
                    ],
                    'pluginEvents' => [
                        'fileuploaded' => 'function(event, data, previewId, index){
                             var response = data.response;
                             if(response.file_name!=""){
                                $("#ArticleImage").val(response.file_name);
                                $(".kv-upload-progress").remove();
                                $(".file-thumb-progress").remove();
                             }else{
                                alert(response.message);
                             }
                             return false;
                        }',
                        'filesuccessremove' => 'function(event, id){}'
                    ]
                ]); ?>
            </div>
        </div>
    </div>

    <div class="form-group highlight-addon">
        <label class="control-label has-star col-md-2"><?php echo $model->getAttributeLabel('poster'); ?></label>
        <div class="col-md-10" id="uploadArticle1">
            <?= Html::activeHiddenInput($model, 'poster', ['id' => 'ArticleImage1']); ?>
            <div class="well-small">
                <?php
                $image = ($model->poster) ? $model->poster : '/img/noimage.gif';
                ?>
                <?= FileInput::widget([
                    'name' => 'file',
                    'pluginOptions' => [
                        'showCaption' => false,
                        'showRemove' => true,
                        'showClose' => false,
                        'showUpload' => true,
                        'browseClass' => 'btn btn-primary',
                        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                        'browseLabel' => 'Select Photo',
                        'uploadUrl' => Url::toRoute(['upload/tmp']),
                        'defaultPreviewContent' => Html::img($image),
                        'maxFileSize' => 1500,
                        'minImageWidth' => 200,
                        'minImageHeight' => 200,
                    ],
                    'options' => [
                        'accept' => 'image/*',
                        'allowedFileExtensions' => ['jpg', 'gif', 'png']
                    ],
                    'pluginEvents' => [
                        'fileuploaded' => 'function(event, data, previewId, index){
                             var response = data.response;
                             if(response.file_name!=""){
                                $("#ArticleImage").val(response.file_name);
                                $(".kv-upload-progress").remove();
                                $(".file-thumb-progress").remove();
                             }else{
                                alert(response.message);
                             }
                             return false;
                        }',
                        'filesuccessremove' => 'function(event, id){}'
                    ]
                ]); ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'keywords')->widget(Select2::classname(), [
        'data' => CUtils::dataTags($model->keywords),
        'options' => ['placeholder' => 'Add tag new ...', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
            'maximumInputLength' => 50
        ],
    ]) ?>

    <?= $form->field($model, 'post_category_id')->dropDownList($list_category, ['prompt' => 'Select....']) ?>

    <?= $form->field($model, 'slug')->textInput() ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

    <?php ActiveForm::end();
    LteBox::end(); ?>

</div>
