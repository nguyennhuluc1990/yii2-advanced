<?php
/* @var $this \yii\web\View */

/* @var $directoryAsset string */
/* @var $username string */
/* @var $avatar string */

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $avatar ?>" class="img-circle" style="max-height: 45px;" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $username ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
                'items' =>$this->context->menuLeftItems,
            ]
        ) ?>

    </section>

</aside>
