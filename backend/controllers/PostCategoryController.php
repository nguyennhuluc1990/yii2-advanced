<?php

namespace backend\controllers;

use common\helpers\CUtils;
use Yii;
use common\models\PostCategory;
use backend\models\PostCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostCategoryController implements the CRUD actions for PostCategory model.
 */
class PostCategoryController extends BaseController {

    /**
     * @inheritdoc
     */
    protected function verbs() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all PostCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PostCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $userModel = $this->findModel($id);

        if ($userModel->load(Yii::$app->request->bodyParams) && $userModel->save()) {
            Yii::$app->session->setFlash('message', Yii::t('app', 'Update User Successfully'));
            return $this->redirect(['view', 'id' => $userModel->id]);
        }
        return $this->render('view', [
            'model' => $userModel,
        ]);
    }

    /**
     * Creates a new PostCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PostCategory();

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->keywords)){
                $model->keywords = implode(',', $model->keywords);
            }
            if(empty($model->slug)){
                $model->slug = CUtils::slugify($model->title);
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        $model->keywords = explode(',', $model->keywords);
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PostCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->keywords)){
                $model->keywords = implode(',', $model->keywords);
            }
            if(empty($model->slug)){
                $model->slug = CUtils::slugify($model->title);
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        $model->keywords = explode(',', $model->keywords);
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PostCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PostCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PostCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PostCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
