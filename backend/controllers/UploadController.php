<?php

namespace backend\controllers;


use common\helpers\ErrorCode;
use common\models\UploadForm;
use Yii;
use yii\web\UploadedFile;

class UploadController extends BaseController
{
    public function actionTmp()
    {

        //$destFolder = getPost('destFolder', '');
        $destFolder='uploads/images';
        // $name = getPost('name', '');
        $name = null;
        if (isset($_FILES['file'])) {
            $name = $_FILES['file'];
        }
        // print_R($_FILES);die;

        $image = UploadedFile::getInstanceByName('file');
        //
        //print_r($image);die;

        // if no image was uploaded abort the upload
        if (empty($image)) {
            echo json_encode(['error' => 'File not found!']);
        } else {
            $arr = explode(".", $image->name);
            $ext = end($arr);

            // $destFolder = str_replace('ewings-run', 'tmp', $destFolder);
            $filename = $destFolder . '/' . time() . '.' . $ext;
            $path = $filename;

            $image->saveAs($path);

            echo json_encode(['full_path' => $path, 'file_name' => '/'.$filename]);
        }

    }
}
