<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Json;
use common\models\User;
use common\models\UserRole;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;

class BaseController extends \yii\web\Controller
{

    public $menuLeftItems = array();

    /**
     * @return array
     */
    public function behaviors()
    {
        Yii::$app->language = (isset($_COOKIE['language']) && $_COOKIE['language'] != 'en') ? 'vi-VN' : 'en-EN';

        $user_id = Yii::$app->user->id;

        $is_access = false;

        $controller_id = Yii::$app->controller->id; //test
        $action_id = Yii::$app->controller->action->id; //index
        //Check if user is login
        if (isset($user_id)) {

            //Get data
            $role_id = User::findOne($user_id)->role_id;
            $permission = Json::decode(UserRole::findOne($role_id)->permission);

            if (ArrayHelper::keyExists($controller_id, $permission)) {
                $is_access = in_array($action_id, $permission[$controller_id]);
            }

            $this->menuLeftItems = [
                [
                    'label' => Yii::t('app', 'Menu'),
                    'icon' => 'users',
                    'url' => '/menu',
                    'visible' => isset($permission['menu']) && in_array('index', $permission['menu']) == true,
                    'active' => strpos(Yii::$app->request->url, 'menu') ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Post Category'),
                    'icon' => 'users',
                    'url' => '/post-category',
                    'visible' => isset($permission['post-category']) && in_array('index', $permission['post-category']) == true,
                    'active' => strpos(Yii::$app->request->url, 'post-category') ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Post'),
                    'icon' => 'users',
                    'url' => '/post',
                    'visible' => isset($permission['post']) && in_array('index', $permission['post']) == true,
                    'active' => strpos(Yii::$app->request->url, 'post') ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Html Content'),
                    'icon' => 'users',
                    'url' => '/html-content',
                    'visible' => isset($permission['html-content']) && in_array('index', $permission['html-content']) == true,
                    'active' => strpos(Yii::$app->request->url, 'html-content') ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'User manager'),
                    'icon' => 'users',
                    'url' => '/user',
                    'visible' => isset($permission['user']) && in_array('index', $permission['user']) == true,
                    'active' => strpos(Yii::$app->request->url, 'user') ? true : false,
                ],
                [
                    'label' => Yii::t('app', 'Permission'),
                    'icon' => 'key',
                    'url' => '/user-role',
                    'visible' => isset($permission['user-role']) && in_array('index', $permission['user-role']) == true,
                    'active' => strpos(Yii::$app->request->url, 'user-role') ? true : false,
                ],
                [
                    'label' => Yii::t('language', 'Language'),
                    'items' => [
                        ['label' => Yii::t('language', 'List of languages'), 'url' => ['/translatemanager/language/list']],
                        ['label' => Yii::t('language', 'Create'), 'url' => ['/translatemanager/language/create']],
                        ['label' => Yii::t('language', 'Scan'), 'url' => ['/translatemanager/language/scan']],
                        ['label' => Yii::t('language', 'Optimize'), 'url' => ['/translatemanager/language/optimizer']],
                        ['label' => Yii::t('language', 'Im-/Export'),
                            'items' => [
                                ['label' => Yii::t('language', 'Import'), 'url' => ['/translatemanager/language/import']],
                                ['label' => Yii::t('language', 'Export'), 'url' => ['/translatemanager/language/export']],
                            ]
                        ]
                    ]
                ],
            ];
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => $is_access,
                            'denyCallback' => function ($rule, $action) {
                                Yii::$app->session->setFlash('error', '403: You are not allowed access this function');
                                if (Yii::$app->request->referrer) {
                                    return $this->redirect(Yii::$app->request->referrer);
                                } else {
                                    return $this->goHome();
                                }
                            },
                            'roles' => ['@'],
                        ]
                    ],
                ]
            ];
        } else {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['login', 'logout', 'error'],
                            'allow' => true,
                            'roles' => ['?'],
                        ],
                        [
                            'allow' => $is_access,
                            'roles' => ['@'],
                        ],
                    ],
                ]
            ];
        }
    }

    /**
     *
     * @return array Danh sach controller
     */
    protected function getControllers()
    {
        $controlers = array();
        $appControllerPath = Yii::$app->getControllerPath();
        //checking existence of controllers directory
        if (is_dir($appControllerPath)) {
            $fileLists = FileHelper::findFiles($appControllerPath);
            //print_R($fileLists);die;
            foreach ($fileLists as $controllerPath) {
                //getting controller name like e.g. 'siteController.php'
                $controllerName = substr($controllerPath, strrpos($controllerPath, DIRECTORY_SEPARATOR) + 1, -4);
                //Clear controller name
                $controllerName = preg_replace("/Controller/", '', $controllerName);
                //Convert to Lower case
                $controllerName = strtolower(preg_replace('/(?<!^)([A-Z])/', '-\\1', $controllerName));
                if ($controllerName != "base") {
                    $controlers[$controllerName] = $this->getActions($controllerName);
                }
            }
        }

        return $controlers;
    }

    /**
     * Get actions of controller
     *
     * @param mixed $controller
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @internal param mixed $module
     */
    protected function getActions($controller)
    {
        $methods = get_class_methods(Yii::$app->createControllerByID($controller));

        // return $methods;
        // var_dump($cInstance->actions());
        $actions = array();
        $pattern = '/^action/';
        foreach ($methods as $method) {
            //preg match start 'action'
            if (strpos($method, 'action') === 0) {
                $actionName = strtolower(preg_replace('/(?<!^)([A-Z])/', '-\\1', str_replace('action', '', $method)));
                $actions[$actionName] = $actionName;
            }
        }
        //Remover Last element actions
        foreach ($actions as $key => $item) {
            if ($item == 's') {
                unset($actions[$key]);
            }
        }

        return $actions;
    }
}
