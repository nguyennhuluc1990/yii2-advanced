<?php
return [
    'fileManagerOptions' => [
        //Upload Manager Configuration
        'configPath' => [
            //path from base_url to base of upload folder with start and final /
            'upload_dir' => '/uploads/filemanager/source/',
            //relative path from filemanager folder to upload folder with final /
            'current_path' => '../../../uploads/filemanager/source/',
            //relative path from filemanager folder to thumbs folder with final / (DO NOT put inside upload folder)
            'thumbs_base_path' => '../../../uploads/filemanager/thumbs/',
            //hidden folders
            'hidden_folders' => ['thumbs']
        ]
    ]
];
