<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\UserRole;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property string $avatar
 * @property integer $birthday
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property integer $role_id
 * @property string $phone
 * @property string $confirm_password
 */
class User extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    public static $arrStatus = [
        self::STATUS_DELETED => 'Chưa kích hoạt',
        self::STATUS_INACTIVE => 'Hủy kích hoạt',
        self::STATUS_ACTIVE => 'Đã kích hoạt',
    ];

    public $confirm_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['email', 'role_id', 'username'], 'required'],
            [['username'], 'unique'],
            [['role_id'], 'integer'],
            ['email', 'email'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['username', 'email', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['avatar', 'username', 'email', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['birthday', 'phone'], 'safe'],
            [['password_hash'], 'required', 'message' => Yii::t('common', 'invalid information, password has more than 6 characters and include a-z; A-Z; 0-9 or special character "!@#$%^&*()'), 'on' => ['create', 'connect', 'reset-password', 'change-password']],
            [['password_hash'], 'string', 'min' => 6, 'message' => Yii::t('common', 'invalid information, password has more than 6 characters and include a-z; A-Z; 0-9 or special character "!@#$%^&*()'), 'on' => ['create', 'connect', 'reset-password', 'change-password']],
            [['old_password'], 'required', 'message' => Yii::t('common', 'invalid information, password has more than 6 characters and include a-z; A-Z; 0-9 or special character "!@#$%^&*()'), 'on' => ['change-password']],
            [['confirm_password'], 'required', 'message' => Yii::t('common', 'invalid information, password confirm has more than 6 characters and include a-z; A-Z; 0-9 or special character "!@#$%^&*()'), 'on' => ['create', 'connect', 'reset-password', 'change-password']],
            [
                'confirm_password',
                'compare',
                'compareAttribute' => 'password_hash',
                'message' => Yii::t('common', 'invalid information, password confirm and password not match '),
                'on' => ['create', 'connect', 'reset-password', 'change-password']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'username' => Yii::t('app', 'Username'),
            'phone' => Yii::t('app', 'Phone'),
            'avatar' => Yii::t('app', 'Avatar'),
            'birthday' => Yii::t('app', 'Birthday'),
            'status' => Yii::t('app', 'Status'),
            'password' => Yii::t('app', 'Password'),
            'confirm_password' => Yii::t('app', 'Confirm Password'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * 
     * @param type $role_id
     */
    public function setRole_id($role_id) {
        $this->role_id = $role_id;
    }

    /**
     * @return integer Role_id
     */
    function getRole_id() {
        return $this->role_id;
    }

    /**
     * 
     * @param type $phone
     */
    public function setPhone($phone) {
        $this->phone;
    }

    /**
     * 
     * @return type
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * 
     * @return type
     */
    public function getUserRole() {
        return $this->hasOne(UserRole::className(), ['id' => 'role_id']);
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword() {
        $this->setPassword($this->password_hash);
        return $this->save(false);
    }

    

}
