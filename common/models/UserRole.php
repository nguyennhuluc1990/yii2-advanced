<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property string $id
 * @property string $name
 * @property string $permission
 *
 * @property User[] $users
 */
class UserRole extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'permission'], 'required'],
            [['permission'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'permission' => Yii::t('app', 'Permission'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }

}
