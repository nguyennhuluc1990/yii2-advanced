<?php
return [
    'upload' => [
        'folder' => 'uploads/filemanager',
        'maxFiles' => 100,
        'maxSize' => 26843545600,
        'extensions' => 'png, jpg',
    ],
    'name' => '102vn.com',
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
