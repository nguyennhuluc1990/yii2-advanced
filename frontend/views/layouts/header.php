<?php

use common\models\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
$menuItems = [];
$menus = Menu::findAll(['status' => Menu::STATUS_ACTIVE]);
foreach ($menus as $menu) {
    $menuItems[] = [
        'label' => $menu->name,
        'url' => $menu->link
    ];
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]);
NavBar::end();
?>