<?php

namespace frontend\controllers;

use common\models\Config;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use common\models\User;
use common\models\UserRole;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;

class BaseController extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $config = Config::find()->one();
//            if (Yii::$app->request->cookies['theme']) {
//                $theme = Yii::$app->request->cookies->getValue('theme');
//            }
            if(!empty($config)){
                $theme = $config->theme;
                Yii::$app->view->theme = new \yii\base\Theme([
                    'pathMap' => ['@app/views' => '@app/themes/'.$theme],
                    'baseUrl' => '@web',

                ]);
            }
            return true;  // or false if needed
        } else {
            return false;
        }
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        Yii::$app->language = (isset($_COOKIE['language']) && $_COOKIE['language'] != 'en') ? 'vi-VN' : 'en-EN';
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     *
     * @return array Danh sach controller
     */
    protected function getControllers()
    {
        $controlers = array();
        $appControllerPath = Yii::$app->getControllerPath();
        //checking existence of controllers directory
        if (is_dir($appControllerPath)) {
            $fileLists = FileHelper::findFiles($appControllerPath);
            //print_R($fileLists);die;
            foreach ($fileLists as $controllerPath) {
                //getting controller name like e.g. 'siteController.php'
                $controllerName = substr($controllerPath, strrpos($controllerPath, DIRECTORY_SEPARATOR) + 1, -4);
                //Clear controller name
                $controllerName = preg_replace("/Controller/", '', $controllerName);
                //Convert to Lower case
                $controllerName = strtolower(preg_replace('/(?<!^)([A-Z])/', '-\\1', $controllerName));
                if ($controllerName != "base") {
                    $controlers[$controllerName] = $this->getActions($controllerName);
                }
            }
        }

        return $controlers;
    }

    /**
     * Get actions of controller
     *
     * @param mixed $controller
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @internal param mixed $module
     */
    protected function getActions($controller)
    {
        $methods = get_class_methods(Yii::$app->createControllerByID($controller));

        // return $methods;
        // var_dump($cInstance->actions());
        $actions = array();
        $pattern = '/^action/';
        foreach ($methods as $method) {
            //preg match start 'action'
            if (strpos($method, 'action') === 0) {
                $actionName = strtolower(preg_replace('/(?<!^)([A-Z])/', '-\\1', str_replace('action', '', $method)));
                $actions[$actionName] = $actionName;
            }
        }
        //Remover Last element actions
        foreach ($actions as $key => $item) {
            if ($item == 's') {
                unset($actions[$key]);
            }
        }

        return $actions;
    }
}
