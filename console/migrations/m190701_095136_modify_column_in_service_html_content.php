<?php

use yii\db\Migration;

/**
 * Class m190701_095136_modify_column_in_service_html_content
 */
class m190701_095136_modify_column_in_service_html_content extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('service', 'created_at', $this->integer(11));
        $this->alterColumn('service', 'updated_at', $this->integer(11));

        $this->alterColumn('html_content', 'created_at', $this->integer(11));
        $this->alterColumn('html_content', 'updated_at', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190701_095136_modify_column_in_service_html_content cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190701_095136_modify_column_in_service_html_content cannot be reverted.\n";

        return false;
    }
    */
}
