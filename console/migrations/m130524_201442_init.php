<?php

use common\models\User;
use common\models\UserRole;
use yii\db\Migration;
use yii\helpers\VarDumper;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'permission' => $this->text()->notNull(),
        ],$tableOptions);

        $userRole = new UserRole();
        $userRole->id = 1;
        $userRole->name = 'admin';
        $userRole->permission = \yii\helpers\Json::encode([
            'user' => ["index", "view", "create", "update", "delete", "reset-password"],
            'user-role' => ["index", "view", "create", "update", "delete"],
        ]);
        if (!$userRole->save()) {
            VarDumper::dump($userRole->getErrors());
            throw new Exception("Cannot insert user_role User!");
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(255),
            'email' => $this->string()->notNull()->unique(),
            'phone' => $this->string(25),
            'role_id' => $this->integer(),
            'avatar' => $this->string(255),
            'birthday' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $user = new User();
        $user->id = 1;
        $user->username = 'admin';
        $user->email = 'lucnn@luci.vn';
        $user->setPassword('admin123');
        $user->auth_key = '';
        $user->role_id = 1;
        if ($user->save()) {
            echo 'User created!\n';
            return 0;
        } else {
            Yii::error($user->getErrors());
            VarDumper::dump($user->getErrors());
            throw new Exception("Cannot create User!");
        }
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
