<?php

use yii\db\Migration;

/**
 * Class m190629_035933_add_column_slug_in_post
 */
class m190629_035933_add_column_slug_in_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post', 'slug', $this->string(255)->notNull());
        $this->addColumn('post_category', 'slug', $this->string(255)->notNull());
        $this->createIndex( 'idx-post-slug','post','slug' );
        $this->createIndex( 'idx-post_category-slug','post_category','slug' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190629_035933_add_column_slug_in_post cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190629_035933_add_column_slug_in_post cannot be reverted.\n";

        return false;
    }
    */
}
