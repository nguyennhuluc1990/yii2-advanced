<?php

use yii\db\Migration;

/**
 * Class m190615_031927_create_post_tables
 */
class m190615_031927_create_post_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'description' => $this->text(),
            'keywords' => $this->string(255),
            'banner' => $this->string(255),
            'poster' => $this->string(255),
            'status' => $this->integer(1)->defaultValue(1)->comment('0 : ẩn, 1 : hiện'),
            'is_deleted' => $this->integer(1)->defaultValue(0)->comment('0 : chưa xóa, 1 : đã xóa'),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $tableOptions);
        $this->createIndex( 'idx-post_category-status','post_category','status' );
        $this->createIndex( 'idx-post_category-is_deleted','post_category','is_deleted' );

        $this->createTable('{{%post}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'content' => $this->text(),
            'banner' => $this->string(255),
            'poster' => $this->string(255),
            'keywords' => $this->string(255),
            'post_category_id' => $this->integer(11),
            'status' => $this->integer(1)->defaultValue(1)->comment('0 : ẩn, 1 : hiện'),
            'is_deleted' => $this->integer(1)->defaultValue(0)->comment('0 : chưa xóa, 1 : đã xóa'),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $tableOptions);
        $this->createIndex( 'idx-post-post_category_id','post','post_category_id' );
        $this->createIndex( 'idx-post-status','post','status' );
        $this->createIndex( 'idx-post-is_deleted','post','is_deleted' );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190615_031927_create_post_tables cannot be reverted.\n";
        $this->dropTable('{{%post_category}}');
        $this->dropTable('{{%post}}');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190615_031927_create_post_tables cannot be reverted.\n";

        return false;
    }
    */
}
