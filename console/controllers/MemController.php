<?php

namespace console\controllers;

use common\models\Menu;
use Exception;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

class MemController extends Controller
{
    public function actionRedis($message = 'xin chao')
    {
        echo $message . "\n";
        $a = Yii::$app->redis->executeCommand('PUBLISH', [
            'channel' => 'chat',
            'message' => Json::encode(['teks' => $message])
        ]);
        var_dump($a);
        die;

    }
    public function actionTest()
    {
//        $m = new \Memcached();
//        $m->addServer('localhost', 11211);
//
//        $m->set('int', 99);
//        $m->set('string', 'a simple string');
//        $m->set('array', array(11, 12));
//        /* expire 'object' key in 5 minutes */
//        $m->set('object', new \stdClass(), time() + 300);
//
//
//        var_dump($m->get('int'));
//        var_dump($m->get('string'));
//        var_dump($m->get('array'));
//        var_dump($m->get('object'));

        $cache = Yii::$app->cache;
        $key = 'Menu1';
        $data = $cache->get($key);
        if ($data === false) {
            echo 'in=====';
            $key = 'Menu1';
            $data = Menu::findOne(['id' => 1]);
            $set = $cache->set($key, $data);
            var_dump($set);
        }
        var_dump($data->name);
    }
}