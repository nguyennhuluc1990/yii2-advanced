<?php

namespace api\controllers;

use common\helpers\ErrorCode;
use api\models\PostCategoryResponse;
use api\models\PostCategorySearch;
use Yii;

class PostCategoryController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * @SWG\Get(
     *      path="/post-category/list",
     *      operationId="PostCategory list",
     *      summary="PostCategory list",
     *      consumes = {"application/json"},
     *      produces = {"application/json"},
     *      tags={"PostCategory"},
     *      @SWG\Parameter(in="header", name="LucnnApiLanguage", required=false, type="string", default="vi-VN", description="Language, format vi-VN, en-US"),
     *      @SWG\Parameter(in="query", name="sort", type="string", default="", description="Sort by:<br/> <b>+-name</b>: Tên PostCategory <br/><b>+-status</b>: Trạng thái"),
     *      @SWG\Response(response=200, description="Info",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean"),
     *              @SWG\Property(property="statusCode", type="integer", default=200),
     *              @SWG\Property(property="data", type="object",
     *                  @SWG\Property(property="items", type="array",
     *                      @SWG\Items(type="object", ref="#/definitions/PostCategoryResponse"),
     *                  ),
     *              ),
     *          ),
     *      ),
     *      security = {
     *         {
     *             "api_app_key": {}
     *         }
     *      },
     * )
     */
    public function actionList()
    {
        $modelSearch = new PostCategorySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        return [
            'items' => $dataProvider->getModels(),
        ];
    }

    /**
     * @SWG\Get(
     *      path="/post-category/detail",
     *      operationId="PostCategory detail",
     *      summary="PostCategory",
     *      consumes = {"application/json"},
     *      produces = {"application/json"},
     *      tags={"PostCategory"},
     *      @SWG\Parameter(in="header", name="LucnnApiLanguage", required=false, type="string", default="vi-VN", description="Language, format vi-VN, en-US"),
     *      @SWG\Parameter( in="query", name="id", type="integer", default="1", description="id PostCategory" ),
     *      @SWG\Response(response=200, description="Info",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean"),
     *              @SWG\Property(property="statusCode", type="integer", default=200),
     *              @SWG\Property(property="data",  type="object", ref="#/definitions/PostCategoryResponse"),
     *          ),
     *      ),
     *      security = {
     *         {
     *             "api_app_key": {}
     *         }
     *      },
     * )
     */
    public function actionDetail($id)
    {
        $res = PostCategoryResponse::findOne(['id' => (int)$id]);
        return $res;
    }

}
