<?php

namespace api\controllers;

use common\helpers\ErrorCode;
use api\models\MenuResponse;
use api\models\MenuSearch;
use Yii;

class MenuController extends ApiController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    /**
     * @SWG\Get(
     *      path="/menu/list",
     *      operationId="Menu list",
     *      summary="Menu list",
     *      consumes = {"application/json"},
     *      produces = {"application/json"},
     *      tags={"Menu"},
     *      @SWG\Parameter(in="header", name="LucnnApiLanguage", required=false, type="string", default="vi-VN", description="Language, format vi-VN, en-US"),
     *      @SWG\Parameter(in="query", name="sort", type="string", default="", description="Sort by:<br/> <b>+-name</b>: Tên Menu <br/><b>+-status</b>: Trạng thái"),
     *      @SWG\Response(response=200, description="Info",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean"),
     *              @SWG\Property(property="statusCode", type="integer", default=200),
     *              @SWG\Property(property="data", type="object",
     *                  @SWG\Property(property="items", type="array",
     *                      @SWG\Items(type="object", ref="#/definitions/MenuResponse"),
     *                  ),
     *              ),
     *          ),
     *      ),
     *      security = {
     *         {
     *             "api_app_key": {}
     *         }
     *      },
     * )
     */
    public function actionList()
    {
        $modelSearch = new MenuSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
        return [
            'items' => $dataProvider->getModels(),
        ];
    }

    /**
     * @SWG\Get(
     *      path="/menu/detail",
     *      operationId="Menu detail",
     *      summary="Menu",
     *      consumes = {"application/json"},
     *      produces = {"application/json"},
     *      tags={"Menu"},
     *      @SWG\Parameter(in="header", name="LucnnApiLanguage", required=false, type="string", default="vi-VN", description="Language, format vi-VN, en-US"),
     *      @SWG\Parameter( in="query", name="id", type="integer", default="1", description="id Menu" ),
     *      @SWG\Response(response=200, description="Info",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="success", type="boolean"),
     *              @SWG\Property(property="statusCode", type="integer", default=200),
     *              @SWG\Property(property="data",  type="object", ref="#/definitions/MenuResponse"),
     *          ),
     *      ),
     *      security = {
     *         {
     *             "api_app_key": {}
     *         }
     *      },
     * )
     */
    public function actionDetail($id)
    {
        $res = MenuResponse::findOne(['id' => (int)$id]);
        return $res;
    }

}
