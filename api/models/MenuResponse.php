<?php

namespace api\models;

use common\models\Menu;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="MenuResponse")
 * )
 */
class MenuResponse extends Menu
{
    /**
     * @SWG\Property(property="id", type="integer"),
     * @SWG\Property(property="name", type="string"),
     * @SWG\Property(property="link", type="string"),
     * @SWG\Property(property="target", type="string"),
     * @SWG\Property(property="type", type="integer"),
     * @SWG\Property(property="parent_id", type="integer"),
     * @SWG\Property(property="updated_at", type="integer"),
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'link',
            'target',
            'type',
            'parent_id',
            'updated_at',
        ];
    }
}
