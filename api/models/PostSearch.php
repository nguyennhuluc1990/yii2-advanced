<?php

namespace api\models;

use common\helpers\CUtils;
use common\models\PostCategory;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Post;

/**
 * PostSearch represents the model behind the search form of `common\models\Post`.
 */
class PostSearch extends Post
{
    public $slug_category;
    public $keyword;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'post_category_id'], 'integer'],
            [['title', 'content', 'description', 'slug_category', 'keyword'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostResponse::find()->where(['is_deleted' => Post::NOT_DELETED]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($params['pageSize']) && $params['pageSize'] > 0 ? (int)$params['pageSize'] : 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'updated_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load(CUtils::modifyParams($params),'');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        if(!empty($this->slug_category)){
            $postCategory = PostCategory::find()->where(['slug' => $this->slug_category])->one();
            if(!empty($postCategory)){
                $this->post_category_id = $postCategory->id;
            }
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'post_category_id' => $this->post_category_id,
        ]);

        $query->andFilterWhere(['or', ['like', 'title', $this->keyword], ['like', 'content', $this->keyword], ['like', 'description', $this->keyword]]);

        return $dataProvider;
    }
}
