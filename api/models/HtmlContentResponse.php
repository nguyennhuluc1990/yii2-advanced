<?php

namespace api\models;

use common\helpers\CUtils;
use common\models\HtmlContent;
use http\Url;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="HtmlContentResponse")
 * )
 */
class HtmlContentResponse extends HtmlContent
{
    /**
     * @SWG\Property(property="id", type="integer"),
     * @SWG\Property(property="code", type="string"),
     * @SWG\Property(property="content", type="string"),
     * @SWG\Property(property="updated_at", type="integer"),
     */
    public function fields()
    {
        return [
            'id',
            'code',
//            'content',
            'content' => function($model){
                return CUtils::replace_img_src('https://' . $_SERVER['HTTP_HOST'], $model->content);
            },
            'updated_at'
        ];
    }
}
