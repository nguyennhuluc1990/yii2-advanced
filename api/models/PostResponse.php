<?php

namespace api\models;

use common\helpers\CUtils;
use common\models\Post;
use http\Url;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="PostResponse")
 * )
 */
class PostResponse extends Post
{
    /**
     * @SWG\Property(property="id", type="integer"),
     * @SWG\Property(property="title", type="string"),
     * @SWG\Property(property="description", type="string"),
     * @SWG\Property(property="content", type="string"),
     * @SWG\Property(property="keywords", type="string"),
     * @SWG\Property(property="banner", type="string"),
     * @SWG\Property(property="poster", type="string"),
     * @SWG\Property(property="post_category_id", type="integer"),
     * @SWG\Property(property="post_category_name", type="string"),
     * @SWG\Property(property="slug", type="string"),
     * @SWG\Property(property="updated_at", type="integer"),
     */
    public function fields()
    {
        return [
            'id',
            'title',
            'description',
//            'content',
            'content' => function ($model) {
                return CUtils::replace_img_src('https://' . $_SERVER['HTTP_HOST'], $model->content);
            },
            'keywords' => function ($model) {
                return explode(',', $model->keywords);
            },
            'banner' => function ($model) {
                if (!empty($model->banner)) {
                    return 'https://' . $_SERVER['HTTP_HOST'] . $model->banner;
                }
                return '';
            },
            'poster' => function ($model) {
                if (!empty($model->poster)) {
                    return 'https://' . $_SERVER['HTTP_HOST'] . $model->poster;
                }
                return '';
            },
            'post_category_id',
            'post_category_name' => function ($model) {
                if (!empty($model->postCategory)) {
                    return $model->postCategory->name;
                }
                return '';
            },
            'slug',
            'updated_at'
        ];
    }
}
