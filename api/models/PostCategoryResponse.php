<?php

namespace api\models;

use common\models\Post;
use common\models\PostCategory;
use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * @SWG\Definition(
 *   type="object",
 *   @SWG\Xml(name="PostCategoryResponse")
 * )
 */
class PostCategoryResponse extends PostCategory
{
    /**
     * @SWG\Property(property="id", type="integer"),
     * @SWG\Property(property="name", type="string"),
     * @SWG\Property(property="description", type="string"),
     * @SWG\Property(property="keywords", type="string"),
     * @SWG\Property(property="banner", type="string"),
     * @SWG\Property(property="poster", type="string"),
     * @SWG\Property(property="slug", type="string"),
     * @SWG\Property(property="count_post", type="integer"),
     * @SWG\Property(property="updated_at", type="integer"),
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'description',
            'keywords' => function ($model) {
                return explode(',', $model->keywords);
            },
            'banner' => function ($model) {
                if (!empty($model->banner)) {
                    return 'https://' . $_SERVER['HTTP_HOST'] . $model->banner;
                }
                return '';
            },
            'poster' => function ($model) {
                if (!empty($model->poster)) {
                    return 'https://' . $_SERVER['HTTP_HOST'] . $model->poster;
                }
                return '';
            },
            'slug',
            'updated_at',
            'count_post' => function($model){
                return (int)Post::find()->where(['post_category_id' => $model->id])->count();
            }
        ];
    }
}
