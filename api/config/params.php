<?php
return [
    'format' => 'json',
    'settings' => [
        'name' => 'Lucnn',
        'title' => 'Lucnn',
        'description' => '',
        'hotline' => '0961196368',
        'email' => 'nguyennhuluc1990@gmail.com',
        'website' => 'https://102vn.com',
    ],
    'agent' => -1,
    'action' => 'swagger',
    'upload' => [
        'folder' => 'uploads/',
        'maxFiles' => 100,
        'maxSize' => 26843545600,
    ],
];
